package com.quizemon.utils;

public class SysEnvUtil {

  public static <T> T getenv(String prop, T defaultValue) {

    T env = defaultValue;

    if(System.getenv(prop) != null) {
      if(defaultValue instanceof Integer) {
        try {
          //Since defaultValue is an Integer, we know T is Integer type.
          env = (T) new Integer(System.getenv(prop));
        } catch (Exception e) {
          System.out.println("Unable to parse environment varaible '" + prop + "' to an integer, defaults to " + defaultValue);
        }
      } else {
        env = (T) System.getenv(prop);
      }
    }

    return env;
  }
}
