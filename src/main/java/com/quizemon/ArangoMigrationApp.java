package com.quizemon;

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDatabase;
import com.arangodb.model.GeoIndexOptions;
import com.quizemon.utils.SysEnvUtil;

import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class ArangoMigrationApp
{
  public static void main( String[] args )
  {
    ArangoDB handle = connectToDB();
    ArangoDatabase db = initDb(handle);

    System.out.println("Migration done!");
    System.exit(0);
  }

  private static ArangoDB connectToDB() {
    // @formatter:off
    String host =     SysEnvUtil.getenv("ARANGO_HOST","localhost");
    Integer port =    SysEnvUtil.getenv("ARANGO_PORT", 8529);
    String user =     SysEnvUtil.getenv("ARANGO_ROOT_USER","root");
    String password = SysEnvUtil.getenv("ARANGO_PASSWORD", "test");
    // @formatter:on

    return new ArangoDB.Builder().host(host, port).user(user).password(password).build();
  }

  private static ArangoDatabase initDb(ArangoDB handle) {

    //One instance of ArangoDB can have multiple databases
    String databaseName = "quizemon3";

    if(!handle.getDatabases().contains(databaseName)) {
      handle.createDatabase(databaseName);
    }

    ArangoDatabase db = handle.db(databaseName);

    if(!db.collection("questions").exists()) {
      db.createCollection("questions");
    }

    if(!db.collection("questionpoints").exists()) {
      db.createCollection("questionpoints");

      GeoIndexOptions options = new GeoIndexOptions();
      options.geoJson(false);
      ArrayList<String> fields = new ArrayList<>(2);
      fields.add("latitude");
      fields.add("longitude");
      db.collection("questionpoints").ensureGeoIndex(fields, options);
    }

    return db;
  }
}
